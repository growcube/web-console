#!/usr/bin/env bash

set -e

apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes rsync
