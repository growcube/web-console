
export const CONN_OPTIONS_DEFAULT_BAUDRATE = 115200;

// name of usb device filter available in filters array (see usb_devices)
export const DEFAULT_PORT_FILTERS_NAMES = ['Micropython (on RPI Pico)', ];
