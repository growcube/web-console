import byteSize from "byte-size";

export function formatBytes(bytesReceived: number): string {
    return byteSize(bytesReceived, {
        precision: 2
    }).toString();
}
