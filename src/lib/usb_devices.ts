import Logger from "./log";

const logger = new Logger("usb-device");

interface IUSBDevice {
    id: number,
    name: string,
}

interface IUSBVendor {
    id: number;
    name: string;
    alias?: string;
    devices?: Array<IUSBDevice>;
}

type TUSBVendorList = Array<IUSBVendor>;

interface IDeviceFilter {
    name: string;
    filters: {
        usbVendorId: number;
        usbProductId?: number;
    }[]
}

export interface SerialPortMetadata {
    label: String;
    info: SerialPortInfo;
}

// https://stackoverflow.com/questions/68194590/get-name-of-connected-device-in-web-serial-api-on-chrome
export function getPortMetadata(port: SerialPort): SerialPortMetadata {
    return {
        label: niceName(port),
        info: port.getInfo(),
    };
}

/**
 * A list of USB vendors and devices for identifying USB devices attached to a serial port.
 */
export const vendors: TUSBVendorList = [
    // FIXME: update vendor list
    {
        id: 0x2e8a,
        name: "RPI Pico",
        alias: "Pico (W?)",
        devices: [
            {
                id: 0x0005,
                name: "Board in FS mode"
            }
        ]
    },
];

export const deviceFilters: IDeviceFilter[] = [
    // FIXME: update the list
    {
        name: "Arduino Portenta C33",
        filters: [
            { usbVendorId: 0x2341, usbProductId: 0x0468 },
        ]
    },
    {
        name: "Olimex RT1010",
        filters: [
            { usbVendorId: 0x15ba, usbProductId: 0x0046 },
        ]
    },
    {
        name: 'Raspberry Pico (W)',
        filters: [
            // https://github.com/raspberrypi/usb-pid
            { usbVendorId: 0x2e8a, usbProductId: 0x0005 },
        ]
    },
];

export function getDeviceFilters(filterNames: string[]): SerialPortFilter[]
{
    let result = [];
    for (const filterName of filterNames) {
        for (const filterGroup of deviceFilters) {
            if (filterGroup.name === filterName) {
                result = [...result, ...filterGroup.filters];
            }
        }
    }

    return result;
}

export function niceName(port: SerialPort, _default?: String): String | null {
    const { usbVendorId, usbProductId } = port.getInfo();

    logger.debug("Detecting nicename, usbProductId: " + usbProductId + " usbVendorId: " + usbVendorId);

    for (const vendor of vendors) {
        if (vendor.id === usbVendorId) {
            for (const device of vendor.devices || []) {
                if (device.id === usbProductId) {
                    return `${vendor.alias || vendor.name} - ${device.name}`
                }
            }

            return vendor.alias || vendor.name;
        }
    }

    return _default || 'unknown device port';
}
