

export function saveConnOptions(connOptions: SerialOptions, prefix: string = ""): void {
    localStorage.setItem((prefix ? prefix + "." : "") + 'connOptions', JSON.stringify(connOptions));
}

export function loadConnOptions(prefix: string = ""): SerialOptions | null {
    const data = localStorage.getItem((prefix ? prefix + "." : "") + "connOptions");
    if (!data) {
        return null
    };

    return JSON.parse(data) as SerialOptions;
}
