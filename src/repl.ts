import { SerialConnection } from "./lib/serial_connection"
import chalk from "chalk"
import { Terminal } from "xterm";
import { FitAddon } from "xterm-addon-fit";
import { Readline } from "./xterm-readline/readline";
import { WebLinksAddon } from "xterm-addon-web-links";
import { Unicode11Addon } from "xterm-addon-unicode11";
import { WebglAddon } from "xterm-addon-webgl";
import { CanvasAddon } from "xterm-addon-canvas";
import Logger from "./lib/log";
import { Input, InputType } from "./xterm-readline/keymap";
import { EventDispatcher } from "./lib/dispatcher";
import { formatBytes } from "./lib/utils";


const baseTheme = {
    foreground: "#F8F8F8",
    background: "#2D2E2C",
    selection: "#5DA5D533",
    black: "#1E1E1D",
    brightBlack: "#262625",
    red: "#CE5C5C",
    brightRed: "#FF7272",
    green: "#5BCC5B",
    brightGreen: "#72FF72",
    yellow: "#CCCC5B",
    brightYellow: "#FFFF72",
    blue: "#5D5DD3",
    brightBlue: "#7279FF",
    magenta: "#BC5ED1",
    brightMagenta: "#E572FF",
    cyan: "#5DA5D5",
    brightCyan: "#72F0FF",
    white: "#F8F8F8",
    brightWhite: "#FFFFFF"
};

const PROMPT_REGEX = /^(?:(?:>>>|\.\.\.|===) *|>)/;
const PROMPT_TYPE_PASTEMODE = 1;
const PROMPT_TYPE_NORMAL = 0;
const PROMPT_TYPE_RAW = 2;

const logger = new Logger("repl");


class Repl {
    private port: SerialPort | undefined;
    private portConn: SerialConnection | undefined;
    private textEncoder: TextEncoder;
    private textDecoder: TextDecoder;
    private promptBuffer: Array<number>;    
    private readonly defaultPromptString: string;
    private dispatcher: EventDispatcher;

    public readonly terminal: Terminal
    public readonly fitAddon: FitAddon
    public readonly rl: Readline
    public isWebglEnabled: boolean;
    public promptString: string;

    constructor(defaultPrompt: string = "", dispatcher: EventDispatcher) {
        this.textEncoder = new TextEncoder();
        this.textDecoder = new TextDecoder();
        this.promptBuffer = [];
        this.dispatcher = dispatcher;
        this.terminal = new Terminal({
            fontFamily: '"JetBrains Mono", monospace',
            fontSize: 14,
            lineHeight: 1.2,
            theme: baseTheme,
            cursorBlink: true,            
            allowProposedApi: true, // required by unicode11 addon
            convertEol: true,
        });

        this.defaultPromptString = defaultPrompt;
        this.promptString = defaultPrompt; // default prompt, the real one is detected from the output
        this.fitAddon = new FitAddon();
        this.rl = new Readline();
        
        this.rl.disable();
        this.rl.setDefaultHandler(this.handleInputCharFromTerminal.bind(this));

        this.terminal.loadAddon(new WebLinksAddon());
        this.terminal.loadAddon(new Unicode11Addon());
        this.terminal.loadAddon(this.fitAddon);
        this.terminal.loadAddon(this.rl);
        this.isWebglEnabled = false;

        try {
          const webgl = new WebglAddon();
          webgl.onContextLoss((e) => {
            webgl.dispose();
          });
          this.terminal.loadAddon(webgl);
          this.isWebglEnabled = true;
        } catch (e) {
          logger.warn("WebGL addon threw an exception during load", e);
        }

        // fallback to canvas rendering as per xterm docs
        if (!this.isWebglEnabled) {
            this.terminal.loadAddon(new CanvasAddon());
        }

        // activate the new version
        this.terminal.unicode.activeVersion = "11";

        window.addEventListener("resize", () => {
            logger.debug("Resizing window");            
            this.fitAddon.fit();

            this.dispatcher.dispatch("window.resized");
        });

        navigator.serial.addEventListener("disconnect", async (event) => {
            if (event.target === this.port) {       
                await this.closePort().catch((e) => {
                    logger.error("Error while disconnecting the port", e)
                });
            }
        });
    }

    private async requestPort(connOptions: SerialOptions, serialPortRequestOptions?: SerialPortRequestOptions) {
        if (this.portConn) {
            return;
        }

        const port = await navigator.serial.requestPort(serialPortRequestOptions);

        if (!port) {
            this.dispatcher.dispatch("port.request-failed");

            return;
        }

        this.dispatcher.dispatch("port.request-ok", { port: this.port });
        this.port = port;
        this.promptBuffer = [];
        this.portConn = new SerialConnection(this.port, this.processIncomingData.bind(this), false);

        try {
            this.dispatcher.dispatch("port.opening");
            logger.debug("Opening port", connOptions);
            await this.portConn.open(connOptions).then(() => {
                this.onPortConnected();
                this.readLineFromTerminal();
                this.dispatcher.dispatch("port.opened", { port: this.port });
            });
        } catch(DOMException) {
            throw DOMException
        }

        await this.portConn.runLoop();
    }

    private async closePort() {
        if (!this.portConn) {
            return;
        }

        this.dispatcher.dispatch("port.closing");

        await this.portConn.close().then(() => {
            this.onPortDisconnected();
            this.dispatcher.dispatch("port.closed");
        }).catch((e) => {
            // make sure that we restore the state in case of an exception
            // FIXME: pass the exception?
            this.onPortDisconnected();
            this.dispatcher.dispatch("port.closed");
        });

        this.portConn = undefined;
    }

    private onPortConnected() {
        this.rl.println(chalk.yellow("\n<CONNECTED>"));
    }
    
    private onPortDisconnected() {
        this.rl.println(chalk.yellow("\n<DISCONNECTED>"));
        this.rl.disable();
        // reset prompt
        this.promptBuffer = [];
        this.promptString = this.defaultPromptString;
    }

    private processIncomingData(v: any) {
        logger.debug("Received data from serial", v);

        const bytes = this.portConn.getBytesRead();
        this.dispatcher.dispatch("serial.bytes-updated", {
            bytes: bytes,
            bytesFormatted: formatBytes(bytes)
        });

        // paste mode prints \n as the last char, skip this
        if (this.detectReplPrompt(v) == PROMPT_TYPE_PASTEMODE) {
            this.terminal.write(v.slice(0, v.length - 1));
        } else {            
           this.terminal.write(v);
        }                
    }

    private detectReplPrompt(v: Uint8Array): number {
        for (let i = 0; i < v.length; i++) {
            let char = v[i];
            switch(char) {
                case 10:
                    this.promptBuffer = [];
                break;
                default:
                    this.promptBuffer.push(char);
                break;
            }
        }

        const s = this.textDecoder.decode(Uint8Array.from(this.promptBuffer));
        const matched = s.match(PROMPT_REGEX);

        if (matched) {
            const prompt = matched[0];           
            if (this.promptString != prompt) {
                logger.debug("Setting prompt to: matched: \"", matched[0], "\"", "prompt: ", this.promptString);
                this.promptString = prompt;                
                // abort active promise, and schedule new with updated prompt string
                this.rl.abortActiveRead();
                setTimeout(this.readLineFromTerminal.bind(this));
            }
        }

        switch(this.promptString)  {
            case "=== ":
                return PROMPT_TYPE_PASTEMODE;
            case ">":
                return PROMPT_TYPE_RAW;
            default:
                return PROMPT_TYPE_NORMAL;
        }        
    }

    private readLineFromTerminal() {
        this.rl.enable();
        this.rl.read(this.promptString).then(this.processTerminalInputLine.bind(this)).catch((e) => {
            console.log("Exception has been thrown in process terminal input line", e)
        });
    }

    private processTerminalInputLine(text: string) {
        // port disconnected
        if (!this.portConn) {
            return;
        }

        this.portConn.write(this.textEncoder.encode(text + "\r\n"));
        setTimeout(this.readLineFromTerminal.bind(this), 0);
    }

    private handleInputCharFromTerminal(input: Input) {
        if (!this.portConn) {
            return;
        }
      
        switch (input.inputType) {
          case InputType.CtrlC:
            this.terminal.write("^C");
          break;
        }
      
        this.portConn.write(this.textEncoder.encode(input.data.join("")));
    }

    public run(element: HTMLElement) {
        this.terminal.open(element!);
        this.terminal.focus();

        this.fitAddon.fit();

        // welcome message
        this.rl.println(chalk.green("Welcome to REPL console. Please connect the device..."));
        this.rl.println("");
        this.rl.println("Select connection parameters and click the connect button.");
        this.rl.println("  * To clean the terminal press CTRL-L");
        this.rl.println("  * To abort running program press CTRL-C");
        this.rl.println("");
        this.rl.println("For more info see https://docs.micropython.org/en/latest/reference/repl.html");
        this.rl.println("");

        this.dispatcher.dispatch("terminal.ready");
    }

    public focus() {
        this.terminal.focus();
    }

    public async connect(connOptions: SerialOptions, serialPortRequestOptions?: SerialPortRequestOptions) {
        await this.requestPort(connOptions, serialPortRequestOptions);
    }

    public async disconnect() {
        await this.closePort();
    }

}

export default Repl;
