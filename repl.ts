import { createApp } from 'vue';
import App from './src/ReplApp.vue';
import ApiNotSupported from './src/SerialApiUnsupported.vue';  

const webAPISerialSupport = "serial" in navigator;
let app: any;

if (webAPISerialSupport) {
    app = createApp(App);    
} else {
    app = createApp(ApiNotSupported);
}

// https://vuejs.org/api/application.html#app-config
// define global properties of the application
// app.config.globalProperties.dispatcher = dispatcher
// console.log(app.config)

// app.config.globalProperties.$hostname = 'http://localhost:3000'

app.mount('#app');
