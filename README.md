# Web Console

[![CI Status](https://gitlab.com/growcube/web-console/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/growcube/web-console/)
[![CI Status](https://gitlab.com/growcube/web-console/-/badges/release.svg)](https://gitlab.com/growcube/web-console/)


Web Console application provides access to Micropython's REPL via WebSerial API. See https://console.growcube.2000cubits.org.

This is a part of my Growcube project (not public yet). I plan to implement more features into this app (display logs, graph serial).

Latest build URL: https://web-console-growcube-22ea95c258b43723813e65dce8311acae45924dd44.gitlab.io/

**Note: The repository on Gitlab.com is a mirror of my private repository, so not all activity might be published there.**

## Does my web browser support it?

WebSerial API is supported in Chromium based browsers since version 89 (see https://caniuse.com/web-serial).

## Build

    git clone https://gitlab.com/growcube/web-console.git
    cd web-console
    npm install
    ./build.sh

## Roadmap

 * Add log formatter (growcube device specific)
 * Add graph plotter
 * Add device configurator (growcube device specific)

## Contributing

Feel free to report issues or feature requests.

## Authors and acknowledgment

The application code is inspired by work of:

 * https://github.com/zaxbux/web-serial-console by Zach Schneider
 * https://github.com/strtok/xterm-readline by Erik Bremen

Thank you!

## License

See LICENSE file.


## Project status

Under development. Please feel free to report issues.
