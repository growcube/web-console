const path = require('path');
var fs = require('fs');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const { VueLoaderPlugin } = require("vue-loader");
const autoprefixer = require("autoprefixer");
const TerserPlugin = require("terser-webpack-plugin");
const CompressionPlugin = require('compression-webpack-plugin')

const decoder = new TextDecoder()

let VERSION = 'n/a';
if (fs.existsSync('VERSION')) {
  VERSION = decoder.decode(fs.readFileSync('VERSION'));
} else {
  VERSION = decoder.decode(fs.readFileSync('VERSION.dist'));
}

// console.log("VERSION", VERSION);

module.exports = {
  entry: {
    repl: [
      './repl.ts',
      './repl.less',
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Growcube ~ REPL",
      // Load a custom template (lodash by default)
      template: "index.html",
      environment: {
        version: VERSION
      },
      options: {
        hash: true,
        inject: true,
        compile: true,
        favicon: false,
        minify: true,
        cache: true,
        showErrors: true,
      }      
    }),  
    new MiniCssExtractPlugin({
      filename: "[name]-[contenthash].min.css"
    }),
    new VueLoaderPlugin(),
    new CompressionPlugin(),
  ],
  mode: "production",
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
      },      
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        options: {
          appendTsSuffixTo: [/\.vue$/]
        }
      },
      {
        test: /\.less$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },          
          // {
          //   loader: "style-loader",
          // },
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },            
          },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                    autoprefixer(),
                ]
              }
            }
          },
          {
            loader: "less-loader",
            options: {
              sourceMap: true,
              lessOptions: {
                strictMath: true,
              },
            },
          },
        ],
      },      
    ],
  },
  optimization: {
    minimizer: [
      new CssMinimizerPlugin(),
      new TerserPlugin({
        test: /\.js$/i,
      }),
    ],
  },  
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.vue'],
  },
  output: {
    filename: "[name]-[contenthash].min.js",
    path: path.resolve(__dirname, 'dist'),
  },
};
